package org.vaxtrac.motech.dhis2.parser;

import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Dhis2RequestParser {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final String TRACKED_ENTITY = "trackedEntity";
	private final String TRACKED_ENTITY_INSTANCE = "trackedEntityInstance";
	private final String DATE_OF_ENROLLMENT = "dateOfEnrollment";
	private final String PROGRAM_STAGE = "programStage";
	private final String ATTRIBUTES = "attributes";
	private final String ATTRIBUTE = "attribute";
	private final String VALUE = "value";
	private final String DATA_ELEMENT = "dataElement";
	private final String DATA_VALUES = "dataValues";
	private final String DOSE = "dose";

	private final String COMMCARE_ID = "zu19DVeVMWJ";

	private JSONObject jsonObj; 
	private EventType type;

	public enum EventType {
		PATIENT_CREATION, 
		PATIENT_ENROLLMENT, 
		DOSE,
		OTHER;
	}

	public Dhis2RequestParser(String json) {
		this.jsonObj = new JSONObject(json);
		this.type = EventType.OTHER;
	}

	public EventType getEventType() {
		Iterator<String> it = jsonObj.keys();
		while (it.hasNext()) {
			String key = it.next();
			switch (key) {
			case TRACKED_ENTITY:
				this.type = EventType.PATIENT_CREATION;
				return type;
			case DATE_OF_ENROLLMENT:
				this.type = EventType.PATIENT_ENROLLMENT;
				return type;
			case PROGRAM_STAGE:
				this.type = EventType.DOSE;
				return type;
			default:
				break;
			}
		}
		return type;
	}

	public void setJsonObj(String json) {
		this.jsonObj = new JSONObject(json);
	}

	public String getCommcarePatientId() {
		if (type.equals(EventType.PATIENT_CREATION)) {
			JSONArray attributes = jsonObj.getJSONArray(ATTRIBUTES);
			for (int i=0; i<attributes.length(); i++) {
				JSONObject obj = attributes.getJSONObject(i);
				String attribute = obj.getString(ATTRIBUTE);
				if (attribute.equals(COMMCARE_ID))
					return obj.getString(VALUE);
			}
		}
		return null;
	}

	public String getDhis2PatientUuid() {
		if (type.equals(EventType.PATIENT_ENROLLMENT) || type.equals(EventType.DOSE)) {
			return jsonObj.getString(TRACKED_ENTITY_INSTANCE);
		}
		return null;
	}

	public String getProgramStage() {
		if (type.equals(EventType.DOSE)) {
			return jsonObj.getString(PROGRAM_STAGE);
		}
		return null;
	}

	public String getDoseNumber() {
		if (type.equals(EventType.DOSE)) {
			JSONArray values = jsonObj.getJSONArray(DATA_VALUES);
			for (int i=0; i<values.length(); i++) {
				JSONObject obj = values.getJSONObject(i);
				String value = obj.getString(VALUE);
				if (value.startsWith(DOSE))
					return value;
			}
		}
		return null;
	}

}
