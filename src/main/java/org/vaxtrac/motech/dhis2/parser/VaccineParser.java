package org.vaxtrac.motech.dhis2.parser;

import java.util.List;

public interface VaccineParser {
	List<Vaccine> parseVaccines(String jsonVaccines);
	Vaccine findMostRecentDose(String jsonVaccines);
}
