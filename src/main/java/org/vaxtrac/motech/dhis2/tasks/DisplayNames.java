package org.vaxtrac.motech.dhis2.tasks;

public final class DisplayNames {
	
    private DisplayNames() {
    }
       
    public static final String VAXTRAC_DISPLAY_NAME = "VaxTrac DHIS2";
    public static final String CASE_ID = "Case ID";
    public static final String RECORDED_VACCINES = "Recorded vaccines";
    public static final String REPORTED_VACCINES = "Reported vaccines";
    public static final String GENDER = "Gender";
    public static final String DATE_OF_ENROLLMENT = "Event date";
    public static final String ORG_UNIT_ID = "Organization unit name";
    public static final String DOB = "Date of birth";
    public static final String PATIENT_NAME = "Patient name";
    public static final String PATIENT_AGE = "Patient age";
    public static final String CONTACT_NUMBER = "Contact number";
    public static final String REMINDER_DATE = "Reminder date";
    public static final String APPOINTMENT_DATE = "Appointment date";
    public static final String CALLBACK_DATE = "Callback date";
    public static final String REMINDER_OFFSET_DAYS = "Reminder offset days";
    public static final String DELIVERY_TIME = "Delivery time";
    public static final String TIMEZONE = "Timezone";
    public static final String HOUR = "Delivery hour";                                                                               
    public static final String MINUTES = "Delivery minutes";
    public static final String MESSAGE = "Message";
    public static final String PATIENT_ID = "Patient id";
    public static final String USER_ID = "User id";
    public static final String VACCINE_NAME = "Vaccine name";
    public static final String SERIES = "Series";
    public static final String VILLAGE = "Village";
    public static final String VILLAGE_NAME = "Village name";
    public static final String RELAIS_NAME = "Relais name";
    public static final String PHONE_NUMBER = "Phone number";
    public static final String ASSIGNED_VILLAGES = "Assigned villages";
    public static final String REMINDER_DAYS = "Reminder days";
    public static final String REMINDER_HOUR = "Reminder hour";
    public static final String CLOSE_CASE = "Close case";
    public static final String GUARDIAN_NAME =  "Guardian name";
    public static final String CHILD_ID =  "Child id";


    public static final String CHECK_IMMUNIZATION_RECORDS = "Check immunization records";
    public static final String COMPLETED_IMMUNIZATION = "Completed immunization";
    public static final String COMPUTE_SMS_REMINDER_DATE = "Compute SMS reminder date";
    public static final String SMS_REMINDER_DATE_COMPUTED = "SMS reminder date computed";
    public static final String TASK_DISABLED = "Disabled task notification";
    public static final String VACCINATION_STAGE = "Vaccination stage";
    public static final String PATIENT_REGISTRATION = "Patient registration";
    public static final String HANDLE_DOSE_CASE = "Handle dose case";
    public static final String HANDLE_PATIENT_CASE = "Handle patient case";
    public static final String STORE_PENDING_DOSE= "Store pending dose";
    public static final String SCHEDULE_RELAIS_SMS = "Schedule relais SMS";
    public static final String LOG_PATIENT = "Log patient";
    public static final String LOG_GUARDIAN = "Log guardian";
    public static final String DATE_MODIFIED = "Date modified";
	public static final String PATIENT_CREATION_AND_ENROLLMENT = "Patient creation and enrollment";
	public static final String PATIENT_ENROLLMENT = "Patient enrollment";

}
