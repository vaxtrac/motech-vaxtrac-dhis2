package org.vaxtrac.motech.dhis2.tasks.builder;

import org.motechproject.tasks.contract.TriggerEventRequest;
import java.util.List;

public interface TriggerBuilder {
	TriggerEventRequest buildTrigger();
}
