package org.vaxtrac.motech.dhis2.tasks.builder;

import java.util.ArrayList;
import java.util.List;

import org.motechproject.tasks.contract.EventParameterRequest;
import org.motechproject.tasks.contract.TriggerEventRequest;
import org.vaxtrac.motech.dhis2.event.EventParams;
import org.vaxtrac.motech.dhis2.event.EventSubjects;
import org.vaxtrac.motech.dhis2.tasks.DisplayNames;

public class VaccinationStageTriggerBuilder implements TriggerBuilder{

    @Override
    public TriggerEventRequest buildTrigger() {
	List<EventParameterRequest> parameterRequests = new ArrayList<EventParameterRequest>();

	parameterRequests.add(new EventParameterRequest(DisplayNames.CASE_ID, EventParams.CASE_ID));
	parameterRequests.add(new EventParameterRequest(DisplayNames.PATIENT_ID, EventParams.PATIENT_ID));
	parameterRequests.add(new EventParameterRequest(DisplayNames.DATE_OF_ENROLLMENT, EventParams.DATE_OF_ENROLLMENT));
	parameterRequests.add(new EventParameterRequest(DisplayNames.USER_ID, EventParams.USER_ID));
	parameterRequests.add(new EventParameterRequest(DisplayNames.VACCINE_NAME, EventParams.VACCINE_NAME));
	parameterRequests.add(new EventParameterRequest(DisplayNames.SERIES, EventParams.SERIES));
	parameterRequests.add(new EventParameterRequest(DisplayNames.PATIENT_AGE, EventParams.PATIENT_AGE));

	TriggerEventRequest trigger = new TriggerEventRequest(
							      DisplayNames.VACCINATION_STAGE, // display name
							      EventSubjects.VACCINATION_STAGE, // subject
							      null, // description
							      parameterRequests // event parameters
							      );

	return trigger;
    }

}
