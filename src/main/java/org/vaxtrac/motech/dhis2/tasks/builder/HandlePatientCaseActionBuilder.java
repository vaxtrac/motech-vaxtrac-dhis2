package org.vaxtrac.motech.dhis2.tasks.builder;

import org.motechproject.tasks.contract.ActionEventRequest;
import org.motechproject.tasks.contract.ActionEventRequestBuilder;
import org.motechproject.tasks.contract.ActionParameterRequest;
import org.motechproject.tasks.contract.ActionParameterRequestBuilder;
import org.vaxtrac.motech.dhis2.event.EventParams;
import org.vaxtrac.motech.dhis2.event.EventSubjects;
import org.vaxtrac.motech.dhis2.tasks.DisplayNames;

import java.util.SortedSet;
import java.util.TreeSet;


public class HandlePatientCaseActionBuilder {
    public ActionEventRequest build() {
		SortedSet<ActionParameterRequest> actionParameters = new TreeSet<ActionParameterRequest>();

		int counter = 0;

		ActionParameterRequestBuilder actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.CASE_ID)
		.setKey(EventParams.CASE_ID)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.DATE_OF_ENROLLMENT)
		.setKey(EventParams.DATE_OF_ENROLLMENT)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.USER_ID)
		.setKey(EventParams.USER_ID)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());

		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.GENDER)
		.setKey(EventParams.GENDER)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());
		
		actionParameterBuilder = new ActionParameterRequestBuilder()
		.setDisplayName(DisplayNames.VILLAGE)
		.setKey(EventParams.VILLAGE)
		.setRequired(true)
		.setOrder(counter++);

		actionParameters.add(actionParameterBuilder.createActionParameterRequest());

		ActionEventRequestBuilder actionEventBuilder = new ActionEventRequestBuilder()
		.setActionParameters(actionParameters)
		.setDisplayName(DisplayNames.HANDLE_PATIENT_CASE)
		.setSubject(EventSubjects.HANDLE_PATIENT_CASE);

		return actionEventBuilder.createActionEventRequest();
	}
}
