package org.vaxtrac.motech.dhis2.tasks.builder;

import java.util.ArrayList;
import java.util.List;

import org.motechproject.tasks.contract.ActionEventRequest;
import org.motechproject.tasks.contract.ChannelRequest;
import org.motechproject.tasks.contract.TriggerEventRequest;
import org.osgi.framework.BundleContext;
import org.vaxtrac.motech.dhis2.tasks.DisplayNames;

/**
 * Builds a channel request from the records in MDS pertaining to the VaxTrac instance schema.
 *OB
 */

public class ChannelRequestBuilder {

    private BundleContext bundleContext;
	
    public ChannelRequestBuilder(BundleContext bundleContext){
	this.bundleContext = bundleContext;
    }

    /**
     * Creates task action event requests for vaccination records checks.
     * @return the new Channel Request
     */
    public ChannelRequest build() {
	CheckVaccinationRecordsActionBuilder checkVaccinationRecordsActionBuilder = new CheckVaccinationRecordsActionBuilder();
	HandleDoseCaseActionBuilder handleDoseCaseActionBuilder = new HandleDoseCaseActionBuilder();
	HandlePatientCaseActionBuilder handlePatientCaseActionBuilder = new HandlePatientCaseActionBuilder();
	
	List<ActionEventRequest> actions = new ArrayList<ActionEventRequest>();
	actions.add(checkVaccinationRecordsActionBuilder.build());
	actions.add(handleDoseCaseActionBuilder.build());
	actions.add(handlePatientCaseActionBuilder.build());
	
	CompleteImmunizationTriggerBuilder completeImmunizationTriggerBuilder = new CompleteImmunizationTriggerBuilder();
	DisabledTaskTriggerBuilder disabledTaskTriggerBuilder = new DisabledTaskTriggerBuilder();
	VaccinationStageTriggerBuilder vaccinationStageTriggerBuilder = new VaccinationStageTriggerBuilder();
	PatientRegistrationTriggerBuilder patientRegistrationTriggerBuilder = new PatientRegistrationTriggerBuilder();

	List<TriggerEventRequest> triggers = new ArrayList<TriggerEventRequest>();
	triggers.add(completeImmunizationTriggerBuilder.buildTrigger());
	triggers.add(disabledTaskTriggerBuilder.buildTrigger());
	triggers.add(vaccinationStageTriggerBuilder.buildTrigger());
	triggers.add(patientRegistrationTriggerBuilder.buildTrigger());
		
	return new ChannelRequest(DisplayNames.VAXTRAC_DISPLAY_NAME, bundleContext.getBundle().getSymbolicName(),
				  bundleContext.getBundle().getVersion().toString(), null, triggers, actions);
    }
}
