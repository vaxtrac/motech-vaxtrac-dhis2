package org.vaxtrac.motech.dhis2.tasks;

import java.util.HashMap;
import java.util.Map;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.joda.time.DateTime;
import org.motechproject.tasks.ex.ValidationException;
import org.motechproject.event.MotechEvent;
import org.motechproject.scheduler.contract.CronSchedulableJob;
import org.motechproject.scheduler.service.MotechSchedulerService;
import org.vaxtrac.motech.dhis2.event.EventSubjects;
import org.vaxtrac.motech.dhis2.service.TasksService;
import org.vaxtrac.motech.dhis2.event.EventParams;

@Component("vaxtracChannelRegistration")
public class VaxtracChannelRegistration {

	private static final Logger LOGGER = LoggerFactory.getLogger(VaxtracChannelRegistration.class);

	private BundleContext bundleContext;
	private TasksService tasksService;
	private MotechSchedulerService schedulerService;
	
	private final String REDELIVERY_JOB_ID = "vaxtrac_dhis2_redelivery_job1";
	private final String REDELIVERY_JOB_CRON_EXPR = "0 0 0 * * ?"; //every day at midnight

	@Autowired
	public VaxtracChannelRegistration(BundleContext bundleContext, TasksService tasksService,
			MotechSchedulerService schedulerService) {
		this.bundleContext = bundleContext;
		this.tasksService = tasksService;
		this.schedulerService = schedulerService;
	}

	public void cleanTasksUp(){
		LOGGER.info("Unschedule dhis2 redelivery job");
		schedulerService.safeUnscheduleJob(EventSubjects.RETRY_FAILED_DELIVERIES, REDELIVERY_JOB_ID);
		
		LOGGER.info("Remove channel");
		tasksService.removeChannel();
	}

	public void updateTasksInfo() {
		LOGGER.info("Schedule dhis2 redelivery job");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(EventParams.JOB_ID, REDELIVERY_JOB_ID);
		CronSchedulableJob cronJob = new CronSchedulableJob(new MotechEvent(EventSubjects.RETRY_FAILED_DELIVERIES, params), 
				REDELIVERY_JOB_CRON_EXPR, DateTime.now().toDate(), null, true);
		schedulerService.safeScheduleJob(cronJob);
		
		LOGGER.info("Updating tasks integration");
		try {
			updateChannel();
		} catch (ValidationException e) {
			LOGGER.error("Channel generated was not accepted by tasks due to validation errors", e);
		}
	}

	public void updateChannel() {
		ServiceReference serviceReference = bundleContext.getServiceReference("org.motechproject.tasks.service.ChannelService");
		if (serviceReference != null) {
			Object service = bundleContext.getService(serviceReference);
			if (service != null) {
				LOGGER.info("Registering VaxTrac - DHIS2 tasks channel with the channel service");
				tasksService.updateChannel();
			} else {
				LOGGER.warn("No channel service present, channel not registered");
			}
		}
	}
}
