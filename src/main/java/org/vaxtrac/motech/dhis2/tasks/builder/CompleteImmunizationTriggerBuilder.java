package org.vaxtrac.motech.dhis2.tasks.builder;

import java.util.ArrayList;
import java.util.List;

import org.motechproject.tasks.contract.EventParameterRequest;
import org.motechproject.tasks.contract.TriggerEventRequest;
import org.vaxtrac.motech.dhis2.event.EventParams;
import org.vaxtrac.motech.dhis2.event.EventSubjects;
import org.vaxtrac.motech.dhis2.tasks.DisplayNames;

public class CompleteImmunizationTriggerBuilder implements TriggerBuilder{

	@Override
	public TriggerEventRequest buildTrigger() {
		List<EventParameterRequest> parameterRequests = new ArrayList<EventParameterRequest>();

		parameterRequests.add(new EventParameterRequest(DisplayNames.CASE_ID, EventParams.CASE_ID));
		parameterRequests.add(new EventParameterRequest(DisplayNames.GENDER, EventParams.GENDER));
		parameterRequests.add(new EventParameterRequest(DisplayNames.DATE_OF_ENROLLMENT, EventParams.DATE_OF_ENROLLMENT));
		parameterRequests.add(new EventParameterRequest(DisplayNames.ORG_UNIT_ID, EventParams.ORG_UNIT_ID));

		TriggerEventRequest trigger = new TriggerEventRequest(
				DisplayNames.COMPLETED_IMMUNIZATION, // display name
				EventSubjects.COMPLETED_IMMUNIZATION, // subject
				null, // description
				parameterRequests // event parameters
				);

		return trigger;
	}

}