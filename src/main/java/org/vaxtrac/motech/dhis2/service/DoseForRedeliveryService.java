package org.vaxtrac.motech.dhis2.service;

import java.util.List;

import org.motechproject.mds.annotations.LookupField;
import org.vaxtrac.motech.dhis2.domain.DoseForRedelivery;

public interface DoseForRedeliveryService {
	  public List<DoseForRedelivery> findAll();
	    public DoseForRedelivery findByCaseId(String caseId);
	    public List<DoseForRedelivery> findByPatientId(String patientId);
	    public DoseForRedelivery findFromHttpRequest(String patientId, String vaccineName, String series);
	    public DoseForRedelivery create(String caseId, String patientId, String patientAge, String eventDate,
			       String userId, String vaccineName, String series);
	    public DoseForRedelivery create(DoseForRedelivery dose);
	    public void update(DoseForRedelivery dose);
	    public void delete(DoseForRedelivery dose);
	    public void deleteAll();
}
