package org.vaxtrac.motech.dhis2.service;

import java.util.List;

import org.vaxtrac.motech.dhis2.domain.PatientForRedelivery;

public interface PatientForRedeliveryService {
	public List<PatientForRedelivery> findAll();
    public PatientForRedelivery findByCaseId(String caseId);
    public PatientForRedelivery create(String caseId, String enrollmentDate,
			String gender, String orgUnitUuid, String village);
    public PatientForRedelivery create();
    public void update(PatientForRedelivery dose);
    public void delete(PatientForRedelivery dose);
    public void deleteAll();
}
