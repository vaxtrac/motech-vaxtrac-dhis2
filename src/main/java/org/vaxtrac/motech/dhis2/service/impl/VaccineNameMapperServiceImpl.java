package org.vaxtrac.motech.dhis2.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaxtrac.motech.dhis2.domain.VaccineNameMapper;
import org.vaxtrac.motech.dhis2.repository.VaccineNameMapperDataService;
import org.vaxtrac.motech.dhis2.service.VaccineNameMapperService;

import java.util.List;

@Service
public class VaccineNameMapperServiceImpl implements VaccineNameMapperService {
    @Autowired
    private VaccineNameMapperDataService vaccineNameMapperDataService;
	
    public VaccineNameMapperServiceImpl(
			VaccineNameMapperDataService vaccineNameMapperDataService) {
		this.vaccineNameMapperDataService = vaccineNameMapperDataService;
	}

	public VaccineNameMapperServiceImpl() {
	}

	@Override
    public List<VaccineNameMapper> findAll() {
	return vaccineNameMapperDataService.retrieveAll();
    }

    @Override
    public VaccineNameMapper findByStageId(String stageId) {
	return vaccineNameMapperDataService.findByStageId(stageId);
    }

    @Override
    public VaccineNameMapper findByVaccineName(String vaccineName) {
	return vaccineNameMapperDataService.findByVaccineName(vaccineName);
    }

    @Override
    public VaccineNameMapper create(String stageId, String vaccineName, String series, String dhis2Name) {
    	return vaccineNameMapperDataService.create(new VaccineNameMapper(stageId, vaccineName, series, dhis2Name));
    }

    @Override
    public VaccineNameMapper create() {
	return vaccineNameMapperDataService.create(new VaccineNameMapper());
    }

    @Override
    public void update(VaccineNameMapper mapper) {
	vaccineNameMapperDataService.update(mapper);
    }

    @Override
    public void delete(VaccineNameMapper mapper) {
	vaccineNameMapperDataService.delete(mapper);	
    }

    @Override
    public void deleteAll() {
	vaccineNameMapperDataService.deleteAll();
    }

    @Override
    public void add(VaccineNameMapper mapper) {
	vaccineNameMapperDataService.create(mapper);
    }

	@Override
	public VaccineNameMapper findByNameandSeries(String vaccineName,
			String series) {
		return vaccineNameMapperDataService.findByNameandSeries(vaccineName, series);
	}

	@Override
	public VaccineNameMapper findByStageIdandDhis2Name(String stageId,
			String series) {
		return vaccineNameMapperDataService.findByStageIdandDhis2Name(stageId, series);
	}
}
