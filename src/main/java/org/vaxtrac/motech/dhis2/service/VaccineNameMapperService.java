package org.vaxtrac.motech.dhis2.service;

import java.util.List;

import org.vaxtrac.motech.dhis2.domain.VaccineNameMapper;

public interface VaccineNameMapperService {
    public List<VaccineNameMapper> findAll();
    public VaccineNameMapper findByStageId(String stageId);
    public VaccineNameMapper findByVaccineName(String vaccineName);
    public VaccineNameMapper findByNameandSeries(String vaccineName, String series);
    public VaccineNameMapper findByStageIdandDhis2Name(String stageId, String series);
    public VaccineNameMapper create();
    public VaccineNameMapper create(String stageId, String vaccineName, String series,
			String dhis2Name);
    public void update(VaccineNameMapper mapper);
    public void delete(VaccineNameMapper mapper);
    public void deleteAll();
    public void add(VaccineNameMapper mapper);   
}
