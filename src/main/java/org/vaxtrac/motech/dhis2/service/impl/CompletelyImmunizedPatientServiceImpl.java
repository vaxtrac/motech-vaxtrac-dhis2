package org.vaxtrac.motech.dhis2.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaxtrac.motech.dhis2.domain.CompletelyImmunizedPatient;
import org.vaxtrac.motech.dhis2.repository.CompletelyImmunizedPatientDataService;
import org.vaxtrac.motech.dhis2.service.CompletelyImmunizedPatientService;

import java.util.List;

@Service
public class CompletelyImmunizedPatientServiceImpl implements CompletelyImmunizedPatientService{
	@Autowired
    private CompletelyImmunizedPatientDataService completelyImmunizedPatientDataService;
	
	public CompletelyImmunizedPatientServiceImpl() {
	}

	public CompletelyImmunizedPatientServiceImpl(
			CompletelyImmunizedPatientDataService completelyImmunizedPatientDataService) {
		this.completelyImmunizedPatientDataService = completelyImmunizedPatientDataService;
	}

	@Override
	public List<CompletelyImmunizedPatient> findAll() {
		return completelyImmunizedPatientDataService.retrieveAll();
	}

	@Override
	public CompletelyImmunizedPatient findByCommcareId(String commcareId) {
		return completelyImmunizedPatientDataService.findByCommcareId(commcareId);
	}

	@Override
	public CompletelyImmunizedPatient create(String commcareId) {
		return completelyImmunizedPatientDataService.create(new CompletelyImmunizedPatient(commcareId));
	}

	@Override
	public void update(CompletelyImmunizedPatient patient) {
		completelyImmunizedPatientDataService.update(patient);
	}

	@Override
	public void delete(CompletelyImmunizedPatient patient) {
		completelyImmunizedPatientDataService.delete(patient);	
	}

	@Override
	public void deleteAll() {
		completelyImmunizedPatientDataService.deleteAll();
	}

	@Override
	public void add(CompletelyImmunizedPatient patient) {
		completelyImmunizedPatientDataService.create(patient);
	}

}
