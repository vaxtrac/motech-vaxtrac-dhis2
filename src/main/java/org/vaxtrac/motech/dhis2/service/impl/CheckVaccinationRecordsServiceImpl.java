package org.vaxtrac.motech.dhis2.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.vaxtrac.motech.dhis2.parser.Vaccine;
import org.vaxtrac.motech.dhis2.parser.VaccineParser;
import org.vaxtrac.motech.dhis2.parser.VaccineParserImpl;
import org.vaxtrac.motech.dhis2.service.CheckVaccinationRecordsService;

@Service
public class CheckVaccinationRecordsServiceImpl implements CheckVaccinationRecordsService{

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final int nbVaccinesToGive = 13;
	private final int yearsAgeLimit = 1;




	@Override
	public boolean patientIsCompletelyImmunized(String reportedVaccines, String recordedVaccines, String dateOfBirth) {
		VaccineParser parser = new VaccineParserImpl();
		List<Vaccine> vaccines = parser.parseVaccines(reportedVaccines);
		vaccines.addAll(parser.parseVaccines(recordedVaccines));
		for (Vaccine vaccine : vaccines) {
			logger.info(vaccine.toString());
		}

		if (vaccines.size() < nbVaccinesToGive)
			return false; 

		try {
			DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
			DateTime dob = formatter.parseDateTime(dateOfBirth);
			DateTime bday = dob.plusYears(yearsAgeLimit);

			for(Vaccine vaccine: vaccines) {
				if (!vaccine.getDate().isBefore(bday) || 
						vaccine.getName().equals("VitaminA") && vaccines.size() == nbVaccinesToGive) {
					return false;
				}			
			}
			return true;
		} catch (Exception e) {
			logger.error("Could not parse date of birth", e);
		}
		return false;
	}

}
