package org.vaxtrac.motech.dhis2.service.impl;

import org.vaxtrac.motech.dhis2.service.TasksService;
import org.vaxtrac.motech.dhis2.tasks.builder.ChannelRequestBuilder;
import org.motechproject.tasks.contract.ChannelRequest;
import org.motechproject.tasks.service.ChannelService;
import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TasksServiceImpl implements TasksService {

	private BundleContext bundleContext;
	private ChannelService channelService;
    private ChannelRequest channelRequest;

	@Autowired
    public TasksServiceImpl(BundleContext bundleContext, ChannelService channelService) {
        this.bundleContext = bundleContext;
        this.channelService = channelService;
    }
	
	@Override
	public void updateChannel() {
		ChannelRequestBuilder channelRequestBuilder = new ChannelRequestBuilder(bundleContext);
		channelRequest = channelRequestBuilder.build();
		channelService.registerChannel(channelRequest);
	}
    
    @Override
    public void removeChannel(){
	channelService.unregisterChannel(channelRequest.getModuleName());
    }
}
