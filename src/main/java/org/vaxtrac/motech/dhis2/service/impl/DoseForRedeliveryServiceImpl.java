package org.vaxtrac.motech.dhis2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaxtrac.motech.dhis2.domain.DoseForRedelivery;
import org.vaxtrac.motech.dhis2.repository.DoseForRedeliveryDataService;
import org.vaxtrac.motech.dhis2.service.DoseForRedeliveryService;

@Service
public class DoseForRedeliveryServiceImpl implements DoseForRedeliveryService {
	@Autowired
	private DoseForRedeliveryDataService doseForRedeliveryDataService;

	public DoseForRedeliveryServiceImpl(DoseForRedeliveryDataService doseForRedeliveryDataService) {
		this.doseForRedeliveryDataService = doseForRedeliveryDataService;
	}

	public DoseForRedeliveryServiceImpl() {
	}

	@Override
	public List<DoseForRedelivery> findAll() {
		return doseForRedeliveryDataService.retrieveAll();
	}

	@Override
	public DoseForRedelivery findByCaseId(String caseId) {
		return doseForRedeliveryDataService.findByCaseId(caseId);
	}

	@Override
	public List<DoseForRedelivery> findByPatientId(String patientId) {
		return doseForRedeliveryDataService.findByPatientId(patientId);
	}

	@Override
	public DoseForRedelivery create(String caseId, String patientId, String patientAge, String eventDate, 
			String userId, String vaccineName, String series) {
		return doseForRedeliveryDataService.create(new DoseForRedelivery(caseId, patientId, patientAge, eventDate,
				userId, vaccineName, series));
	}

	@Override
	public DoseForRedelivery create(DoseForRedelivery dose) {
		return doseForRedeliveryDataService.create(dose);
	}

	@Override
	public void update(DoseForRedelivery dose) {
		doseForRedeliveryDataService.update(dose);
	}

	@Override
	public void delete(DoseForRedelivery dose) {
		doseForRedeliveryDataService.delete(dose);	
	}

	@Override
	public void deleteAll() {
		doseForRedeliveryDataService.deleteAll();
	}

	@Override
	public DoseForRedelivery findFromHttpRequest(String patientId,
			String vaccineName, String series) {
		return doseForRedeliveryDataService.findFromHttpRequest(patientId, vaccineName, series);
	}

}
