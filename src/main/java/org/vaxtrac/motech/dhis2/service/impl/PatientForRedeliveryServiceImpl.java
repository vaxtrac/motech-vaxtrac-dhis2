package org.vaxtrac.motech.dhis2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaxtrac.motech.dhis2.domain.PatientForRedelivery;
import org.vaxtrac.motech.dhis2.repository.PatientForRedeliveryDataService;
import org.vaxtrac.motech.dhis2.service.PatientForRedeliveryService;

public class PatientForRedeliveryServiceImpl implements PatientForRedeliveryService {
	@Autowired
	private PatientForRedeliveryDataService patientForRedeliveryDataService;

	public PatientForRedeliveryServiceImpl(PatientForRedeliveryDataService patientForRedeliveryDataService) {
		this.patientForRedeliveryDataService = patientForRedeliveryDataService;
	}

	public PatientForRedeliveryServiceImpl() {
	}

	@Override
	public List<PatientForRedelivery> findAll() {
		return patientForRedeliveryDataService.retrieveAll();
	}

	@Override
	public PatientForRedelivery findByCaseId(String caseId) {
		return patientForRedeliveryDataService.findByCaseId(caseId);
	}

	@Override
	public PatientForRedelivery create(String caseId, String enrollmentDate,
			String gender, String orgUnitUuid, String village) {
		return patientForRedeliveryDataService.create(new PatientForRedelivery(caseId, enrollmentDate,
				gender, orgUnitUuid, village));
	}

	@Override
	public PatientForRedelivery create() {
		return patientForRedeliveryDataService.create(new PatientForRedelivery());
	}

	@Override
	public void update(PatientForRedelivery patient) {
		patientForRedeliveryDataService.update(patient);
	}

	@Override
	public void delete(PatientForRedelivery patient) {
		patientForRedeliveryDataService.delete(patient);	
	}

	@Override
	public void deleteAll() {
		patientForRedeliveryDataService.deleteAll();
	}
}
