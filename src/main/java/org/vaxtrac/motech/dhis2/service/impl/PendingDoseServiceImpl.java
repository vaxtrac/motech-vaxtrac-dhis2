package org.vaxtrac.motech.dhis2.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaxtrac.motech.dhis2.domain.PendingDose;
import org.vaxtrac.motech.dhis2.repository.PendingDoseDataService;
import org.vaxtrac.motech.dhis2.service.PendingDoseService;

import java.util.List;

@Service
public class PendingDoseServiceImpl implements PendingDoseService{
    @Autowired
    private PendingDoseDataService pendingDoseDataService;
    
    public PendingDoseServiceImpl(PendingDoseDataService pendingDoseDataService) {
		this.pendingDoseDataService = pendingDoseDataService;
	}

	public PendingDoseServiceImpl() {
	}

	@Override
    public List<PendingDose> findAll() {
	return pendingDoseDataService.retrieveAll();
    }

    @Override
    public PendingDose findByCaseId(String caseId) {
	return pendingDoseDataService.findByCaseId(caseId);
    }

    @Override
    public List<PendingDose> findByPatientId(String patientId) {
	return pendingDoseDataService.findByPatientId(patientId);
    }

    @Override
    public PendingDose findByPatientInfo(String patientId, String eventDate, String vaccineName) {
	return pendingDoseDataService.findByPatientInfo(patientId, eventDate, vaccineName);
    }

    @Override
    public PendingDose create(String caseId, String patientId, String patientAge, String eventDate, 
			      String userId, String vaccineName, String series) {
	return pendingDoseDataService.create(new PendingDose(caseId, patientId, patientAge, eventDate,
							     userId, vaccineName, series));
    }

    @Override
    public PendingDose create() {
	return pendingDoseDataService.create(new PendingDose());
    }

    @Override
    public void update(PendingDose dose) {
	pendingDoseDataService.update(dose);
    }

    @Override
    public void delete(PendingDose dose) {
	pendingDoseDataService.delete(dose);	
    }

    @Override
    public void deleteAll() {
	pendingDoseDataService.deleteAll();
    }

    @Override
    public void add(PendingDose dose) {
	pendingDoseDataService.create(dose);
    }

}
