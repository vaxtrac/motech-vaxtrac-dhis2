package org.vaxtrac.motech.dhis2.service;

import java.util.List;

import org.vaxtrac.motech.dhis2.domain.PendingDose;

public interface PendingDoseService {
    public List<PendingDose> findAll();
    public PendingDose findByCaseId(String caseId);
    public List<PendingDose> findByPatientId(String patientId);
    public PendingDose findByPatientInfo(String patientId, String eventDate, String vaccineName);
    public PendingDose create(String caseId, String patientId, String patientAge, String eventDate,
		       String userId, String vaccineName, String series);
    public PendingDose create();
    public void update(PendingDose dose);
    public void delete(PendingDose dose);
    public void deleteAll();
    public void add(PendingDose dose);
}
