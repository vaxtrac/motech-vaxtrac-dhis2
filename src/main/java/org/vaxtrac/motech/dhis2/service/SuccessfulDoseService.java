package org.vaxtrac.motech.dhis2.service;

import java.util.List;

import org.vaxtrac.motech.dhis2.domain.SuccessfulDose;

public interface SuccessfulDoseService {
    public SuccessfulDose findByCaseId(String caseId);
    public List<SuccessfulDose> findByPatientId(String patientId);
    public SuccessfulDose create(String caseId, String patientId);
    public SuccessfulDose create(SuccessfulDose dose);
    public void update(SuccessfulDose dose);
    public void delete(SuccessfulDose dose);
    public void deleteAll();
}
