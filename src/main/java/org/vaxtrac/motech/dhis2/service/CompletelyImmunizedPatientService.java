package org.vaxtrac.motech.dhis2.service;

import java.util.List;

import org.vaxtrac.motech.dhis2.domain.CompletelyImmunizedPatient;

public interface CompletelyImmunizedPatientService {
	List<CompletelyImmunizedPatient> findAll();
	CompletelyImmunizedPatient findByCommcareId(String commcareId);
	CompletelyImmunizedPatient create(String commcareId);
	void update(CompletelyImmunizedPatient patient);
	void delete(CompletelyImmunizedPatient patient);
	void deleteAll();
	void add(CompletelyImmunizedPatient patient);
}
