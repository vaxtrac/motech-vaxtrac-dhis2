package org.vaxtrac.motech.dhis2.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaxtrac.motech.dhis2.domain.SuccessfulDose;
import org.vaxtrac.motech.dhis2.repository.SuccessfulDoseDataService;
import org.vaxtrac.motech.dhis2.service.SuccessfulDoseService;

import java.util.List;

@Service
public class SuccessfulDoseServiceImpl implements SuccessfulDoseService{
    @Autowired
    private SuccessfulDoseDataService successfulDoseDataService;
    
    public SuccessfulDoseServiceImpl(
			SuccessfulDoseDataService successfulDoseDataService) {
		this.successfulDoseDataService = successfulDoseDataService;
	}

	public SuccessfulDoseServiceImpl() {
	}

	@Override
    public SuccessfulDose findByCaseId(String caseId) {
	return successfulDoseDataService.findByCaseId(caseId);
    }

    @Override
    public List<SuccessfulDose> findByPatientId(String patientId) {
    	return successfulDoseDataService.findByPatientId(patientId);
    }

    @Override
    public SuccessfulDose create(String caseId, String patientId) {
	return successfulDoseDataService.create(new SuccessfulDose(caseId, patientId));
    }

    @Override
    public SuccessfulDose create(SuccessfulDose dose) {
	return successfulDoseDataService.create(dose);
    }

    @Override
    public void update(SuccessfulDose dose) {
	successfulDoseDataService.update(dose);
    }

    @Override
    public void delete(SuccessfulDose dose) {
	successfulDoseDataService.delete(dose);	
    }

    @Override
    public void deleteAll() {
	successfulDoseDataService.deleteAll();
    }
}
