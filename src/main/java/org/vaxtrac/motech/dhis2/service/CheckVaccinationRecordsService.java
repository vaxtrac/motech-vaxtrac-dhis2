package org.vaxtrac.motech.dhis2.service;

public interface CheckVaccinationRecordsService {
	boolean patientIsCompletelyImmunized(String reportedVaccines, String recordedVaccines, String dateOfBirth);
}
