package org.vaxtrac.motech.dhis2.repository;

import org.vaxtrac.motech.dhis2.domain.VaccineNameMapper;
import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;

public interface VaccineNameMapperDataService extends MotechDataService<VaccineNameMapper>{
    @Lookup
    VaccineNameMapper findByStageId(@LookupField(name = "stageId") String stageId);

    @Lookup
    VaccineNameMapper findByVaccineName(@LookupField(name = "vaccineName") String vaccineName);
    
    @Lookup
    VaccineNameMapper findByNameandSeries(@LookupField(name = "vaccineName") String vaccineName,
    		@LookupField(name = "series") String series);
    
    @Lookup
    VaccineNameMapper findByStageIdandDhis2Name(@LookupField(name = "stageId") String stageId,
    		@LookupField(name = "dhis2Name") String series);
}
