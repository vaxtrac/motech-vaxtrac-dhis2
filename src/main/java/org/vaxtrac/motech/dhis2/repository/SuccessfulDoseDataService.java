package org.vaxtrac.motech.dhis2.repository;

import org.vaxtrac.motech.dhis2.domain.SuccessfulDose;
import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;

import java.util.List;

public interface SuccessfulDoseDataService extends MotechDataService<SuccessfulDose>{
    @Lookup
    SuccessfulDose findByCaseId(@LookupField(name = "caseId") String caseId);

    @Lookup
    List<SuccessfulDose> findByPatientId(@LookupField(name = "patientId") String patientId);
}
