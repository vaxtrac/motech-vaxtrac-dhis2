package org.vaxtrac.motech.dhis2.repository;

import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;
import org.vaxtrac.motech.dhis2.domain.PatientForRedelivery;

public interface PatientForRedeliveryDataService extends MotechDataService<PatientForRedelivery> {
	@Lookup
	 PatientForRedelivery findByCaseId(@LookupField(name = "caseId") String caseId);
}
