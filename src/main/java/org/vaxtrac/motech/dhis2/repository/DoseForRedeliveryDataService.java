package org.vaxtrac.motech.dhis2.repository;

import java.util.List;

import org.vaxtrac.motech.dhis2.domain.DoseForRedelivery;
import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;

public interface DoseForRedeliveryDataService extends MotechDataService<DoseForRedelivery> {
	 @Lookup
	 DoseForRedelivery findByCaseId(@LookupField(name = "caseId") String caseId);

	 @Lookup
	 List<DoseForRedelivery> findByPatientId(@LookupField(name = "patientId") String patientId);
	 
	 @Lookup
	 DoseForRedelivery findFromHttpRequest(@LookupField(name = "patientId") String patientId,
			 @LookupField(name = "vaccineName") String vaccineName,
			 @LookupField(name = "series") String series);
}
