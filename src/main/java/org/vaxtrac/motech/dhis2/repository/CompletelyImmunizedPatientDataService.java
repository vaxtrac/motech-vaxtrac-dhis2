package org.vaxtrac.motech.dhis2.repository;

import org.vaxtrac.motech.dhis2.domain.CompletelyImmunizedPatient;
import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;

public interface CompletelyImmunizedPatientDataService extends MotechDataService<CompletelyImmunizedPatient>{
	 @Lookup
	 CompletelyImmunizedPatient findByCommcareId(@LookupField(name = "commcareId") String commcareId);
}
