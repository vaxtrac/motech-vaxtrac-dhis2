package org.vaxtrac.motech.dhis2.repository;

import org.vaxtrac.motech.dhis2.domain.PendingDose;
import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;

import java.util.List;

public interface PendingDoseDataService extends MotechDataService<PendingDose>{
    @Lookup
    PendingDose findByCaseId(@LookupField(name = "caseId") String caseId);

    @Lookup
    List<PendingDose> findByPatientId(@LookupField(name = "patientId") String patientId);
    
    @Lookup
    PendingDose findByPatientInfo(@LookupField(name = "patientId") String patientId,
				  @LookupField(name = "eventDate") String eventDate,
				  @LookupField(name = "vaccineName") String vaccineName);
}
