package org.vaxtrac.motech.dhis2.domain;

import org.motechproject.mds.annotations.Entity;
import org.motechproject.mds.annotations.Field;

import java.util.Objects;

/**
 * Models data for simple records in a portable manner.
 */
@Entity
public class CompletelyImmunizedPatient {

	@Field
	private String commcareId;

	public CompletelyImmunizedPatient(String commcareId) {
		this.commcareId = commcareId;
	}

	public String getCommcareId() {
		return commcareId;
	}

	public void setCommcareId(String commcareId) {
		this.commcareId = commcareId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		final CompletelyImmunizedPatient other = (CompletelyImmunizedPatient) obj;

		return Objects.equals(this.commcareId, other.commcareId) && Objects.equals(this.commcareId, other.commcareId);
	}

	@Override
	public String toString() {
		return commcareId;
	}
}
