package org.vaxtrac.motech.dhis2.domain;

import java.util.Objects;

import javax.jdo.annotations.Unique;

import org.motechproject.mds.annotations.Entity;
import org.motechproject.mds.annotations.Field;

@Entity
public class DoseForRedelivery {
	@Field
	@Unique
	private String caseId;

	@Field
	private String patientId;

	@Field
	private String patientAge;

	@Field
	private String eventDate;

	@Field
	private String userId;

	@Field
	private String vaccineName;

	@Field
	private String series;

	@Field
	private int numberOfRedeliveries;

	@Field
	private String errorMessage;

	public DoseForRedelivery() {
		this.errorMessage = null;
		this.numberOfRedeliveries = 0;
	}

	public DoseForRedelivery(String caseId, String patientId, String patientAge, String eventDate, String userId, 
			String vaccineName, String series) {
		this.caseId = caseId;
		this.patientId = patientId;
		this.patientAge = patientAge;
		this.eventDate = eventDate;
		this.userId = userId;
		this.vaccineName = vaccineName;
		this.series = series;
		this.numberOfRedeliveries = 0;
		this.errorMessage = null;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String string) {
		this.caseId = string;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String string) {
		this.patientId = string;
	}
	
	public String getPatientAge() {
		return patientAge;
	}

	public void setPatientAge(String patientAge) {
		this.patientAge = patientAge;
	}
	
	public void setEventDate(String string) {
		this.eventDate = string;
	}
	public String getEventDate() {
		return eventDate;
	}

	public void setUserId(String string) {
		this.userId = string;
	}
	public String getUserId() {
		return userId;
	}

	public void setVaccineName(String string) {
		this.vaccineName = string;
	}

	public String getVaccineName() {
		return vaccineName;
	}

	public void setSeries(String series) {
		this.series = series;
	}
	public String getSeries() {
		return series;
	}

	public void setNumberOfRedeliveries(int number) {
		this.numberOfRedeliveries = number;
	}

	public int getNumberOfRedeliveries() {
		return numberOfRedeliveries;
	}

	public void incrementNumberOfRedeliveries() {
		numberOfRedeliveries++;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorCode) {
		this.errorMessage = errorCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		final DoseForRedelivery other = (DoseForRedelivery) obj;

		return Objects.equals(this.caseId, other.caseId);
	}

}
