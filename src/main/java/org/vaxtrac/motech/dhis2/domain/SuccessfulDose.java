package org.vaxtrac.motech.dhis2.domain;

import org.motechproject.mds.annotations.Entity;
import org.motechproject.mds.annotations.Field;
import javax.jdo.annotations.Unique;

import java.util.Objects;

/**
 * Models data for simple records in a portable manner.
 */
@Entity
public class SuccessfulDose {

    @Field
    @Unique
    private String caseId;

    @Field
    private String patientId;


    public SuccessfulDose(String caseId, String patientId) {
	this.caseId = caseId;
	this.patientId = patientId;
    }

    public String getCaseId() {
	return caseId;
    }

    public void setCaseId(String id) {
	this.caseId = id;
    }

    
    public String getPatientId() {
	return patientId;
    }

    public void setPatientId(String id) {
	this.patientId = id;
    }

    @Override
    public boolean equals(Object obj) {
	if (obj == null || getClass() != obj.getClass()) {
	    return false;
	}

	final SuccessfulDose other = (SuccessfulDose) obj;
	return Objects.equals(this.caseId, other.caseId);
    }
}
