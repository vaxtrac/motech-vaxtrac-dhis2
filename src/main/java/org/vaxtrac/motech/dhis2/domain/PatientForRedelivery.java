package org.vaxtrac.motech.dhis2.domain;

import java.util.Objects;

import javax.jdo.annotations.Unique;

import org.motechproject.mds.annotations.Entity;
import org.motechproject.mds.annotations.Field;

@Entity
public class PatientForRedelivery {
	@Field
    @Unique
    private String caseId;
	
	@Field
	private String enrollmentDate;
	
	@Field
	private String gender;
	
	@Field
	private String userId;
	
	@Field
	private String village;
	
	@Field
	private int numberOfRedeliveries;

	@Field
	private String errorMessage;

	public PatientForRedelivery(String caseId, String enrollmentDate,
			String gender, String orgUnitUuid, String village) {
		this.caseId = caseId;
		this.enrollmentDate = enrollmentDate;
		this.gender = gender;
		this.userId = orgUnitUuid;
		this.village = village;
		this.numberOfRedeliveries = 0;
		this.errorMessage = null;
	}
	
	public PatientForRedelivery() {}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public String getEnrollmentDate() {
		return enrollmentDate;
	}

	public void setEnrollmentDate(String enrollmentDate) {
		this.enrollmentDate = enrollmentDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public int getNumberOfRedeliveries() {
		return numberOfRedeliveries;
	}

	public void incrementNumberOfRedeliveries() {
		numberOfRedeliveries++;
	}

	@Override
    public boolean equals(Object obj) {
	if (obj == null || getClass() != obj.getClass()) {
	    return false;
	}

	final PatientForRedelivery other = (PatientForRedelivery) obj;

	return Objects.equals(this.caseId, other.caseId);
    }
}
