package org.vaxtrac.motech.dhis2.domain;

import org.motechproject.mds.annotations.Entity;
import org.motechproject.mds.annotations.Field;

import java.util.Objects;

/**
 * Models data for simple records in a portable manner.
 */
@Entity
public class VaccineNameMapper {

    @Field
    private String stageId;

    @Field
    private String vaccineName;
    
    @Field
    private String series;
    
    @Field
    private String dhis2Name;

    public VaccineNameMapper() {
    }

    public VaccineNameMapper(String stageId, String vaccineName, String series,
			String dhis2Name) {
		this.stageId = stageId;
		this.vaccineName = vaccineName;
		this.series = series;
		this.dhis2Name = dhis2Name;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getDhis2Name() {
		return dhis2Name;
	}

	public void setDhis2Name(String dhis2Name) {
		this.dhis2Name = dhis2Name;
	}

	public String getStageId() {
	return stageId;
    }

    public void setStageId(String string) {
	this.stageId = string;
    }
    
    public void setVaccineName(String string) {
	this.vaccineName = string;
    }
    
    public String getVaccineName() {
	return vaccineName;
    }

        @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}

	if (obj == null || getClass() != obj.getClass()) {
	    return false;
	}

	final VaccineNameMapper other = (VaccineNameMapper) obj;

	return Objects.equals(this.stageId, other.stageId) &&
	    Objects.equals(this.dhis2Name, other.dhis2Name);
    }
}
