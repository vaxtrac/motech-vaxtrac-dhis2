package org.vaxtrac.motech.dhis2.event;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.motechproject.dhis2.domain.TrackedEntityInstanceMapping;
import org.motechproject.dhis2.service.TrackedEntityInstanceMappingService;
import org.motechproject.event.MotechEvent;
import org.motechproject.event.listener.annotations.MotechListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaxtrac.motech.dhis2.domain.DoseForRedelivery;
import org.vaxtrac.motech.dhis2.domain.PatientForRedelivery;
import org.vaxtrac.motech.dhis2.domain.PendingDose;
import org.vaxtrac.motech.dhis2.domain.SuccessfulDose;
import org.vaxtrac.motech.dhis2.domain.VaccineNameMapper;
import org.vaxtrac.motech.dhis2.parser.Dhis2RequestParser;
import org.vaxtrac.motech.dhis2.parser.Dhis2RequestParser.EventType;
import org.vaxtrac.motech.dhis2.service.CheckVaccinationRecordsService;
import org.vaxtrac.motech.dhis2.service.CompletelyImmunizedPatientService;
import org.vaxtrac.motech.dhis2.service.DoseForRedeliveryService;
import org.vaxtrac.motech.dhis2.service.PatientForRedeliveryService;
import org.vaxtrac.motech.dhis2.service.PendingDoseService;
import org.vaxtrac.motech.dhis2.service.SuccessfulDoseService;
import org.vaxtrac.motech.dhis2.service.VaccineNameMapperService;

/**
 * Event Handler for the VaxTrac module. The public methods listen for the event subjects listed in
 * {@link org.vaxtrac.motech.dhis2.event.EventSubjects}
 *
 */
@Service
public class EventHandler {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EventEmitter eventEmitter;

	@Autowired
	private CheckVaccinationRecordsService checkVaccinationRecordsService;

	@Autowired
	private CompletelyImmunizedPatientService completelyImmunizedPatientDataService;

	@Autowired
	private PendingDoseService pendingDoseService;

	@Autowired
	private SuccessfulDoseService successfulDoseService;

	@Autowired
	private DoseForRedeliveryService doseForRedeliveryService;

	@Autowired
	private PatientForRedeliveryService patientForRedeliveryService;

	@Autowired
	private VaccineNameMapperService vaccineNameMapperService;

	@Autowired
	private TrackedEntityInstanceMappingService dhis2MappingService;

	public EventHandler(
			EventEmitter eventEmitter,
			CheckVaccinationRecordsService checkVaccinationRecordsService,
			CompletelyImmunizedPatientService completelyImmunizedPatientDataService,
			PendingDoseService pendingDoseService,
			SuccessfulDoseService successfulDoseService,
			VaccineNameMapperService vaccineNameMapperService,
			TrackedEntityInstanceMappingService dhis2MappingService,
			PatientForRedeliveryService patientForRedeliveryService,
			DoseForRedeliveryService doseForRedeliveryService) {
		this.eventEmitter = eventEmitter;
		this.checkVaccinationRecordsService = checkVaccinationRecordsService;
		this.completelyImmunizedPatientDataService = completelyImmunizedPatientDataService;
		this.pendingDoseService = pendingDoseService;
		this.successfulDoseService = successfulDoseService;
		this.vaccineNameMapperService = vaccineNameMapperService;
		this.dhis2MappingService = dhis2MappingService;
		this.patientForRedeliveryService = patientForRedeliveryService;
		this.doseForRedeliveryService = doseForRedeliveryService;
	}

	public EventHandler() { }

	@MotechListener(subjects = {EventSubjects.TASK_MODULE_NOTIFICATION})
	public void handleTaskModuleNotification(MotechEvent event) {
		String message = "Received an event with subject " + event.getSubject();
		logger.info(message);

		Map<String, Object> params = new HashMap<String, Object>(event.getParameters());
		message = (String) params.remove(EventParams.MESSAGE);
		eventEmitter.emitDisabledTaskEvent(message);
	}

	@MotechListener(subjects = {EventSubjects.CHECK_IMMUNIZATION_RECORDS})
	public void handleCheckImmunizationRecords(MotechEvent event) {
		String message = "Received an event with subject " + event.getSubject();
		logger.info(message);

		Map<String, Object> params = new HashMap<String, Object>(event.getParameters());
		String caseId = (String) params.remove(EventParams.CASE_ID);
		String dateOfBirth = (String) params.remove(EventParams.DOB);
		String recordedVaccines = (String) params.remove(EventParams.RECORDED_VACCINES);
		String reportedVaccines = (String) params.remove(EventParams.REPORTED_VACCINES);
		String dateOfEnrollment = (String) params.remove(EventParams.DATE_OF_ENROLLMENT);
		String orgUnitId = (String) params.remove(EventParams.ORG_UNIT_ID);

		if (checkVaccinationRecordsService.patientIsCompletelyImmunized(reportedVaccines, recordedVaccines, dateOfBirth)) {
			message = "Patient " + caseId + " completely immunized";
			logger.info(message);
			completelyImmunizedPatientDataService.create(caseId);
			eventEmitter.emitCompletedImmunizationEvent(caseId, dateOfEnrollment, orgUnitId);
		}

		message = "Handled an event with subject " + event.getSubject();
		logger.info(message);
	}

	@MotechListener(subjects = {EventSubjects.HANDLE_DOSE_CASE})
	public void handleDoseCase(MotechEvent event) {
		String message = "Received an event with subject " + event.getSubject();
		logger.info(message);

		Map<String, Object> params = new HashMap<String, Object>(event.getParameters());

		String caseId = (String) params.get(EventParams.CASE_ID);

		SuccessfulDose success = successfulDoseService.findByCaseId(caseId);
		PendingDose pending = pendingDoseService.findByCaseId(caseId);

		if (success == null && pending == null) {
			String patientId = (String) params.get(EventParams.PATIENT_ID);

			String timestamp = (String) params.get(EventParams.DATE_OF_ENROLLMENT);
			if (timestamp.contains("T")) {
				timestamp = timestamp.substring(0, 8) + "0" + timestamp.substring(8, 9);
				params.put(EventParams.DATE_OF_ENROLLMENT, timestamp);
			}

			TrackedEntityInstanceMapping patient =  dhis2MappingService.findByExternalId(patientId);

			String patientAge = (String) params.get(EventParams.PATIENT_AGE);
			String userId = (String) params.get(EventParams.USER_ID);
			String vaccineName = (String) params.get(EventParams.VACCINE_NAME);
			String series = (String) params.get(EventParams.SERIES);

			if (patient != null && patient.getEnrollmentStatus()) {
				DoseForRedelivery dose = doseForRedeliveryService.findByCaseId(caseId);
				if (dose == null)
					doseForRedeliveryService.create(caseId, patientId, patientAge, timestamp, userId, vaccineName, series);
				eventEmitter.emitVaccinationStageEvent(params);
			}
			else {
				pendingDoseService.create(caseId, patientId, patientAge, timestamp, userId, vaccineName, series);
			}
		}

		logger.info("Handled an event with subject " + event.getSubject());
	}

	@MotechListener(subjects = {EventSubjects.HANDLE_PATIENT_CASE})
	public void handlePatientCase(MotechEvent event) {
		String message = "Received an event with subject " + event.getSubject();
		logger.info(message);

		Map<String, Object> eventParams = new HashMap<String, Object>(event.getParameters());
		String patientId = (String) eventParams.get(EventParams.CASE_ID);

		PatientForRedelivery patient = patientForRedeliveryService.findByCaseId(patientId);
		TrackedEntityInstanceMapping mapping = dhis2MappingService.findByExternalId(patientId);
		
		if (patient == null && (mapping == null || !mapping.getEnrollmentStatus())) {
			String enrollmentDate = (String) eventParams.get(EventParams.DATE_OF_ENROLLMENT);
			String gender = (String) eventParams.get(EventParams.GENDER);
			String userId = (String) eventParams.get(EventParams.USER_ID);
			String village = (String) eventParams.get(EventParams.VILLAGE);

			patientForRedeliveryService.create(patientId, enrollmentDate, gender, userId, village);
			eventEmitter.emitPatientRegistrationEvent(patientId, enrollmentDate, gender, userId, village);
		}

		logger.info("Handled an event with subject " + event.getSubject());
	}   


	@MotechListener(subjects = {EventSubjects.ENROLLMENT_SUCCESS})
	public void handleEnrollmentSuccess(MotechEvent event) {
		String message = "Received an event with subject " + event.getSubject();
		logger.info(message);

		Map<String, Object> params = new HashMap<String, Object>(event.getParameters());
		String patientId = (String) params.remove(EventParams.EXTERNAL_ID);

		//Patient for redelivery has been successfully delivered
		PatientForRedelivery patient = patientForRedeliveryService.findByCaseId(patientId);
		if (patient != null) {
			patientForRedeliveryService.delete(patient);
		}

		//Dose cases that arrived before patient cases

		List<PendingDose> pendingDoses = pendingDoseService.findByPatientId(patientId); 

		Iterator<PendingDose> it = pendingDoses.iterator();

		while (it.hasNext()) {
			PendingDose dose = it.next();

			logger.debug("Pending dose: " + dose.getPatientId() + ", " + dose.getEventDate() +  ", " + dose.getVaccineName() + " " + dose.getSeries());

			SuccessfulDose success = successfulDoseService.findByCaseId(dose.getCaseId());
			DoseForRedelivery doseForRedelivery = doseForRedeliveryService.findByCaseId(dose.getCaseId());
			if (success == null && doseForRedelivery == null) {
				Map<String, Object> eventParams = new HashMap<String, Object>();
				eventParams.put(EventParams.CASE_ID, dose.getCaseId());
				eventParams.put(EventParams.PATIENT_ID, dose.getPatientId());
				eventParams.put(EventParams.USER_ID, dose.getUserId());
				eventParams.put(EventParams.VACCINE_NAME, dose.getVaccineName());
				eventParams.put(EventParams.SERIES, dose.getSeries());
				eventParams.put(EventParams.PATIENT_AGE, dose.getPatientAge());
				eventParams.put(EventParams.DATE_OF_ENROLLMENT, dose.getEventDate());

				doseForRedeliveryService.create(dose.getCaseId(), patientId, dose.getPatientId(),
						dose.getEventDate(), dose.getUserId(), dose.getVaccineName(), dose.getSeries());

				pendingDoseService.delete(dose);

				eventEmitter.emitVaccinationStageEvent(eventParams);
			}		
		}

		logger.info("Handled an event with subject " + event.getSubject());
	}   

	@MotechListener(subjects = {EventSubjects.STAGE_UPDATE_SUCCESS})
	public void handleStageUpdateSuccess(MotechEvent event) {
		String message = "Received an event with subject " + event.getSubject();
		logger.info(message);

		Map<String, Object> params = new HashMap<String, Object>(event.getParameters());
		String patientId = (String) params.remove(EventParams.EXTERNAL_ID);
		String caseId = (String) params.remove(EventParams.DOSE_CASE_ID);

		DoseForRedelivery doseForRedelivery = doseForRedeliveryService.findByCaseId(caseId);
		if (doseForRedelivery != null) {
			doseForRedeliveryService.delete(doseForRedelivery);
			logger.debug("Deleted dose: " + patientId + ", " + caseId);
		}

		SuccessfulDose success = successfulDoseService.findByCaseId(caseId);
		if (success == null) { 
			successfulDoseService.create(caseId, patientId);
		}

		logger.info("Handled an event with subject " + event.getSubject());
	}

	@MotechListener(subjects = {EventSubjects.FAILED_DHIS2_DELIVERY})
	public void handleFailedDhis2Delivery(MotechEvent event) {
		String message = "Received an event with subject " + event.getSubject();
		logger.info(message);

		Map<String, Object> params = new HashMap<String, Object>(event.getParameters());
		String errorMessage = (String) params.get(EventParams.ERROR_MESSAGE);
		String requestBody = (String) params.get(EventParams.REQUEST_BODY);

		Dhis2RequestParser parser = new Dhis2RequestParser(requestBody);
		EventType eventType = parser.getEventType();

		switch(eventType) {

		case PATIENT_CREATION:
			PatientForRedelivery patient = patientForRedeliveryService.findByCaseId(parser.getCommcarePatientId());
			if (patient != null) {
				patient.setErrorMessage(errorMessage);
				patientForRedeliveryService.update(patient);
			}
			break;

		case PATIENT_ENROLLMENT:
			TrackedEntityInstanceMapping mapping2 = dhis2MappingService.findByDhis2Uuid(parser.getDhis2PatientUuid());
			if (mapping2 != null) {
				PatientForRedelivery patient2 = patientForRedeliveryService.findByCaseId(mapping2.getExternalName());
				if (patient2 != null) {
					patient2.setErrorMessage(errorMessage);
					patientForRedeliveryService.update(patient2);
				}
			}
			break;

		case DOSE:
			TrackedEntityInstanceMapping mapping = dhis2MappingService.findByDhis2Uuid(parser.getDhis2PatientUuid());
			if (mapping != null) {
				String patientId = mapping.getExternalName();
				VaccineNameMapper vaccineMapper = vaccineNameMapperService.findByStageIdandDhis2Name(
						parser.getProgramStage(), parser.getDoseNumber());
				if (vaccineMapper != null) {
					DoseForRedelivery dose = doseForRedeliveryService.findFromHttpRequest(
							patientId, vaccineMapper.getVaccineName(), vaccineMapper.getSeries());
					if (dose != null) {
						dose.setErrorMessage(errorMessage);
						doseForRedeliveryService.update(dose);
					}
				}
			}
			break;

		default:
			break;
		}

		logger.info("Handled an event with subject " + event.getSubject());
	}

	@MotechListener(subjects = {EventSubjects.RETRY_FAILED_DELIVERIES})
	public void handleRetryFailedDeliveries(MotechEvent event) {
		String message = "Received an event with subject " + event.getSubject();
		logger.info(message);

		//Emit patient cases for creation or enrollment
		List<PatientForRedelivery> patients = patientForRedeliveryService.findAll();
		for (PatientForRedelivery patient : patients) {
			if (patient.getErrorMessage() != null) {
				patient.incrementNumberOfRedeliveries();
				patientForRedeliveryService.update(patient);
				eventEmitter.emitPatientRegistrationEvent(patient.getCaseId(), patient.getEnrollmentDate(),
						patient.getGender(), patient.getUserId(), patient.getVillage());
			}
		}

		//Emit dose cases again
		List<DoseForRedelivery> doses = doseForRedeliveryService.findAll();

		for (DoseForRedelivery dose : doses) {
			if (dose.getErrorMessage() != null) {
				dose.incrementNumberOfRedeliveries();
				doseForRedeliveryService.update(dose);
				Map<String, Object> eventParams = new HashMap<String, Object>();
				eventParams.put(EventParams.CASE_ID, dose.getCaseId());
				eventParams.put(EventParams.PATIENT_ID, dose.getPatientId());
				eventParams.put(EventParams.USER_ID, dose.getUserId());
				eventParams.put(EventParams.VACCINE_NAME, dose.getVaccineName());
				eventParams.put(EventParams.SERIES, dose.getSeries());
				eventParams.put(EventParams.PATIENT_AGE, dose.getPatientAge());
				eventParams.put(EventParams.DATE_OF_ENROLLMENT, dose.getEventDate());
				eventEmitter.emitVaccinationStageEvent(eventParams);
			}
		}

		logger.info("Handled an event with subject " + event.getSubject());
	}
}
