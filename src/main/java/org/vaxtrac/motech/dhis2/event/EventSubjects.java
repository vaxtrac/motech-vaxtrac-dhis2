package org.vaxtrac.motech.dhis2.event;

public final class EventSubjects {
    private EventSubjects() {
    }
    
    public static final String BASE = "org.vaxtrac.motech.dhis2.";
    
    //Actions
    public static final String CHECK_IMMUNIZATION_RECORDS = BASE + "check_immunization_records";
    public static final String TASK_MODULE_NOTIFICATION = "org.motechproject.message";
    public static final String HANDLE_DOSE_CASE = BASE + "handle_dose_case";
    public static final String HANDLE_PATIENT_CASE = BASE + "handle_patient_case";
    public static final String STAGE_UPDATE_SUCCESS = "org.motechproject.dhis2.stage_update_success";
    public static final String STAGE_UPDATE_FAILURE = "org.motechproject.dhis2.stage_update_failure";
    public static final String ENROLLMENT_SUCCESS = "org.motechproject.dhis2.enrollment_success";
    public static final String FAILED_DHIS2_DELIVERY = "org.motechproject.dhis2.failed_dose_delivery";
    public static final String RETRY_FAILED_DELIVERIES = BASE + "retry_failed_deliveries";

    //Triggers
    public static final String COMPLETED_IMMUNIZATION = BASE + "completed_immunization";
    public static final String TASK_DISABLED = BASE + "task_disabled";
    public static final String VACCINATION_STAGE = BASE + "vaccination_stage";
    public static final String PATIENT_REGISTRATION = BASE + "patient_registration";
    public static final String STORE_PENDING_DOSE= BASE + "store_pending_dose";
}
