package org.vaxtrac.motech.dhis2.event;

import org.motechproject.event.MotechEvent;
import org.motechproject.event.listener.EventRelay;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
public class EventEmitter {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EventRelay eventRelay;

	public EventEmitter() { }

	public EventEmitter(EventRelay eventRelay) {
		this.eventRelay = eventRelay;
	}

	public void emitEvent(String subject, Map<String, Object> eventParams) {
		MotechEvent motechEvent = new MotechEvent(subject, eventParams);
		eventRelay.sendEventMessage(motechEvent);
		String message = "Emitted an event with subject " + subject;
		logger.info(message);
	}

	public void emitDisabledTaskEvent(String message) {
		Map<String, Object> eventParams = new HashMap<String, Object>();
		eventParams.put(EventParams.MESSAGE, message);
		emitEvent(EventSubjects.TASK_DISABLED, eventParams);
	}

	public void emitCompletedImmunizationEvent(String caseId, String dateOfEnrollment, String orgUnitId) {
		Map<String, Object> eventParams = new HashMap<String, Object>();
		eventParams.put(EventParams.CASE_ID, caseId);
		eventParams.put(EventParams.DATE_OF_ENROLLMENT, dateOfEnrollment);
		eventParams.put(EventParams.ORG_UNIT_ID, orgUnitId);
		//eventParams.put(EventParams.PATIENT_AGE, patientAge);
		emitEvent(EventSubjects.COMPLETED_IMMUNIZATION, eventParams);
	}

	public void emitVaccinationStageEvent(Map<String, Object> eventParams) {
		logger.trace("Emitted dose: " + (String)eventParams.get(EventParams.PATIENT_ID) + ", " + (String)eventParams.get(EventParams.DATE_OF_ENROLLMENT) + ", " + (String)eventParams.get(EventParams.VACCINE_NAME) + " " + (String)eventParams.get(EventParams.SERIES));
		emitEvent(EventSubjects.VACCINATION_STAGE, eventParams);
	}

	public void emitPatientRegistrationEvent(String patientId, String enrollmentDate, String gender, String userId, String village) {
		Map<String, Object> eventParams = new HashMap<String, Object>();
		eventParams.put(EventParams.CASE_ID, patientId);
		eventParams.put(EventParams.DATE_OF_ENROLLMENT, enrollmentDate);
		eventParams.put(EventParams.GENDER, gender);
		eventParams.put(EventParams.USER_ID, userId);
		eventParams.put(EventParams.VILLAGE, village);
		emitEvent(EventSubjects.PATIENT_REGISTRATION, eventParams);
	}
}
